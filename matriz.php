<form action="matriz.php" method="post" >

<center>
<p>Matriz Quadrada</p> 

<hr/style="width:200px;">
<input type="text" maxlength="2" style="width:22px;" name=a>

<input type="text" maxlength="2" style="width:22px;" name=b>

<input type="text" maxlength="2" style="width:22px;" name=c>

<input type="text" maxlength="2" style="width:22px;" name=d>
<BR><BR>

<input type="text" maxlength="2" style="width:22px;" name=e>

<input type="text" maxlength="2" style="width:22px;" name=f>

<input type="text" maxlength="2" style="width:22px;" name=g>

<input type="text" maxlength="2" style="width:22px;" name=h>
<BR><BR>

<input type="text" maxlength="2" style="width:22px;" name=q>

<input type="text" maxlength="2" style="width:22px;" name=r>

<input type="text" maxlength="2" style="width:22px;" name=k>

<input type="text" maxlength="2" style="width:22px;" name=l>
<BR><BR>

<input type="text" maxlength="2" style="width:22px;" name=s>

<input type="text" maxlength="2" style="width:22px;" name=n>

<input type="text" maxlength="2" style="width:22px;" name=o>

<input type="text" maxlength="2" style="width:22px;" name=p>

<BR>
<hr/ style="width:200px;">
<input type=submit value="Calcular">
 </center>
</form>


<?php
/**
 * Calcular determinante de Matriz NxN
 * Utilizando método Gauss-Jordan (escalonamento / triangularização)
 * @author  Glauber Portella <glauberportella@gmail.com>
 */
class DeterminanteNxN {

	/**
	 * Calcula determinante de matriz quadrada
	 * @param  array $matriz Matriz quadrada
	 * @return float
	 */
	public static function calcular(&$matriz)
	{
		$det 	= 1; // resultado
		$n 		= count($matriz); // dimensao da matriz
		$sinal 	= 1; // controlar sinal do determinante
		$m 		= 1; // controlar operacao de multiplicacao de linha por constante

		for ($i = 0; $i < $n; $i++) {
			if ($matriz[$i][$i] === 0) {
				for ($j = $i + 1; $j < $n; $j++) {
					if ($matriz[$j][$i] !== 0) {
						for ($k = 0; $k < $n; $k++) {
							$temp = $matriz[$i][$k];
							$matriz[$i][$k] = $matriz[$j][$k];
							$matriz[$j][$k] = $temp;
							$sinal *= -1;
						}
					}
				} // for ($j = $i + 1 ...)
			} // if ($matriz[i][i] == 0)

			if ($matriz[$i][$i] !== 1 && $matriz[$i][$i] !== -1) {
				$temp = 1 / $matriz[$i][$i];
				for ($j = $i; $j < $n; $j++) {
					$matriz[$i][$j] = $matriz[$i][$j] * $temp;
				}
				$m *= $temp;
			}

			for ($j = $i + 1; $j < $n; $j++) {
				if ($matriz[$j][$i] === 0)
					continue;

				if ($matriz[$i][$i] * $matriz[$j][$i] >= 0) {
					$matriz[$j][$i] = -1 * $matriz[$j][$i] * $matriz[$i][$i] + $matriz[$j][$i];
				} else {
					$matriz[$j][$i] = $matriz[$j][$i] * $matriz[$i][$i] - $matriz[$j][$i];
				}
			}
		} // for ($i = 0 ...)

		for ($i = 0; $i < $n; $i++) {
			$det *= $matriz[$i][$i];
		}
		
		$det *= $sinal;
		$det /= $m;

		return $det;
	}

	public static function imprime($matriz)
	{
		$n = count($matriz);
		for ($i = 0; $i < $n; $i++) {
			for ($j = 0; $j < $n; $j++) {
				echo sprintf("%4.0f", $matriz[$i][$j]);
				if ($j + 1 === $n)
					echo "<br>";
			}
		}
	}

}

$a=$b=$c=$d=$e=$f=$g=$h=$w=$x=$k=$l=$y=$n=$o=$p=1;
$a = $_POST["a"];
$b = $_POST["b"];
$c = $_POST["c"];
$d = $_POST["d"];
$e = $_POST["e"];
$f = $_POST["f"];
$g = $_POST["g"];
$h = $_POST["h"];
$q = $_POST["q"];
$r = $_POST["r"];
$k = $_POST["k"];
$l = $_POST["l"];
$s = $_POST["s"];
$n = $_POST["n"];
$o = $_POST["o"];
$p = $_POST["p"];
//Caso o valor digitado seja nulo
if($a == null)$a = 1;
if($b == null)$b = 1;
if($c == null)$c = 1;
if($d == null)$d = 1;
if($e == null)$e = 1;
if($f == null)$f = 1;
if($g == null)$g = 1;
if($h == null)$h = 1;
if($q == null)$q = 1;
if($r == null)$r = 1;
if($k == null)$k = 1;
if($l == null)$l = 1;
if($s == null)$s = 1;
if($n == null)$n = 1;
if($o == null)$o = 1;
if($p == null)$p = 1;

$matriz = [
	[$a,$b,$c,$d],
	[$e,$f,$g,$h],
	[$q,$r,$k,$l],
	[$s,$n,$o,$p]
];

echo "<br>Matriz:<br>";
DeterminanteNxN::imprime($matriz);
echo "<br>------------------------------------------";
$det = DeterminanteNxN::calcular($matriz);
echo "<br>Matriz triangular:<br><br>";
DeterminanteNxN::imprime($matriz);
echo "<br>------------------------------------------<br>";
echo "\n\nDeterminante = $det\n\n";
